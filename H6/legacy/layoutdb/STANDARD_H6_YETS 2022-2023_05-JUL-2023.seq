

  /**********************************************************************************
  *
  * H6 version (draft) YETS 2022-2023 in MAD X SEQUENCE format
  * Generated the 05-JUL-2023 02:15:00 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.H6_LSX__FWP                  := 0.7;
l.H6_MBNH_HWP                  := 5;
l.H6_MBNV_HWP                  := 5;
l.H6_MBXHCCWP                  := 2.5;
l.H6_MCXCAHWC                  := 0.4;
l.H6_MSN                       := 3.2;
l.H6_MTN__HWP                  := 3.6;
l.H6_QNL__8WP                  := 2.99;
l.H6_QSL__8WP                  := 3;
l.H6_QTS__8WP                  := 1.49;
l.H6_QWL__8WP                  := 2.948;
l.H6_TBACA                     := 0.25;
l.H6_TBID                      := 0.25;
l.H6_TCMAA                     := 0.4;
l.H6_XCEDN                     := 6.317;
l.H6_XCET_001                  := 0.6;
l.H6_XCHV_001                  := 1;
l.H6_XCON_009                  := 1;
l.H6_XCON_014                  := 0.018;
l.H6_XCSH_001                  := 1;
l.H6_XCSV_001                  := 1;
l.H6_XDWC                      := 0.06;
l.H6_XEMC_001                  := 0.8;
l.H6_XFFH                      := 0.276;
l.H6_XFFV                      := 0.276;
l.H6_XSCI                      := 0.1;
l.H6_XTAX_005                  := 1.615;
l.H6_XTAX_006                  := 1.615;
l.H6_XTDV_001                  := 3.2;
l.H6_XVWAD001                  := 0.0001;
l.H6_XVWAE001                  := 0.0002;
l.H6_XVWAL001                  := 0.00012;
l.H6_XVWAM001                  := 0.00012;
l.H6_XWCA                      := 0.06;

//---------------------- COLLIMATOR     ---------------------------------------------
H6_TCMAA       : COLLIMATOR  , L := l.H6_TCMAA;          ! Collimation mask type A
H6_XCHV_001    : COLLIMATOR  , L := l.H6_XCHV_001;       ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
H6_XCON_009    : COLLIMATOR  , L := l.H6_XCON_009;       ! Converter Type 009 - TARGET01 Lead 3 mm and 6 mm, Poly. 1 m, Copper 400 mm
H6_XCON_014    : COLLIMATOR  , L := l.H6_XCON_014;       ! Converter Type 014 - Lead Absorber 4 mm, 8 mm, 18 mm
H6_XCSH_001    : COLLIMATOR  , L := l.H6_XCSH_001;       ! SPS Collimator a fente horizontal (design 1970)
H6_XCSV_001    : COLLIMATOR  , L := l.H6_XCSV_001;       ! SPS Collimator a fente vertical (design 1970)
H6_XTAX_005    : COLLIMATOR  , L := l.H6_XTAX_005;       ! Target Absorber Type 005
H6_XTAX_006    : COLLIMATOR  , L := l.H6_XTAX_006;       ! Target Absorber Type 006
H6_XTDV_001    : COLLIMATOR  , L := l.H6_XTDV_001;       ! Mobile Dump Vertical (Hydraulic)
//---------------------- HKICKER        ---------------------------------------------

//---------------------- INSTRUMENT     ---------------------------------------------
H6_TBACA       : INSTRUMENT  , L := l.H6_TBACA;          ! Target Box variant A
H6_TBID        : INSTRUMENT  , L := l.H6_TBID;           ! target beam instrumentation, downstream
H6_XCEDN       : INSTRUMENT  , L := l.H6_XCEDN;          ! Cherenkov Differential Counter Nord
H6_XCET_001    : INSTRUMENT  , L := l.H6_XCET_001;       ! Cherenkov Counter DN159 - Mylar Window
H6_XEMC_001    : INSTRUMENT  , L := l.H6_XEMC_001;       ! ElectroMagnetic Calorimeter
H6_XFFH        : INSTRUMENT  , L := l.H6_XFFH;           ! Finger Scintillator Profile Monitor - Horizontal
H6_XFFV        : INSTRUMENT  , L := l.H6_XFFV;           ! Finger Scintillator Profile Monitor - Vertical
H6_XVWAD001    : INSTRUMENT  , L := l.H6_XVWAD001;       ! X Vacuum Window Aluminium DN159X0.1, aperture 120 mm
H6_XVWAE001    : INSTRUMENT  , L := l.H6_XVWAE001;       ! X Vacuum Window Aluminium EL900X60X0.2
H6_XVWAL001    : INSTRUMENT  , L := l.H6_XVWAL001;       ! X Vacuum Window Mylar DN120x70x0.12
H6_XVWAM001    : INSTRUMENT  , L := l.H6_XVWAM001;       ! X Vacuum Window Mylar + pumping port DN40 DN120x100x0.12
//---------------------- KICKER         ---------------------------------------------
H6_MCXCAHWC    : KICKER      , L := l.H6_MCXCAHWC;       ! Corrector magnet, H or V, type MDX
//---------------------- MONITOR        ---------------------------------------------
H6_XDWC        : MONITOR     , L := l.H6_XDWC;           ! Delay Wire Chamber (Beam Profile Monitors)
H6_XSCI        : MONITOR     , L := l.H6_XSCI;           ! Ensemble: Cadre et Scintillateur BXSCI
H6_XWCA        : MONITOR     , L := l.H6_XWCA;           ! Multi Wire Proportional Chamber Assembly
//---------------------- QUADRUPOLE     ---------------------------------------------
H6_QNL__8WP    : QUADRUPOLE  , L := l.H6_QNL__8WP;       ! Quadrupole, secondary beams, type north area
H6_QSL__8WP    : QUADRUPOLE  , L := l.H6_QSL__8WP;       ! Quadrupole, slim, long, - Same magnet type SPQSLD_
H6_QTS__8WP    : QUADRUPOLE  , L := l.H6_QTS__8WP;       ! Quadrupole, BT line, short - Same magnet type as SPQTSD_
H6_QWL__8WP    : QUADRUPOLE  , L := l.H6_QWL__8WP;       ! Quadrupole, Secondary Beams, West Area Type
//---------------------- RBEND          ---------------------------------------------
H6_MBNH_HWP    : RBEND       , L := l.H6_MBNH_HWP;       ! Bending magnet, secondary beams, horizontal, north area
H6_MBNV_HWP    : RBEND       , L := l.H6_MBNV_HWP;       ! Bending magnet, secondary beams, vertical, north area - Mechanical dimensions from NORMA
H6_MBXHCCWP    : RBEND       , L := l.H6_MBXHCCWP;       ! Bending Magnet, H or V, type HB1, 2.5m gap 80mm
H6_MTN__HWP    : RBEND       , L := l.H6_MTN__HWP;       ! Bending magnet, Target N
H6_MSN         : RBEND       , L := l.H6_MSN;            ! Septum magnet, north area - offset mechanical and optical dimensions according to drawing 1766138 (not well indicated)
//---------------------- SEXTUPOLE      ---------------------------------------------
H6_LSX__FWP    : SEXTUPOLE   , L := l.H6_LSX__FWP;       ! Sextupole lens, north area


/************************************************************************************/
/*                       H6 SEQUENCE                                                */
/************************************************************************************/

H6 : SEQUENCE, refer = centre,          L = 555.197;
 TBACA.X0400000                : H6_TBACA        , at = 0.125        , slot_id = 56964667;
 TBID.241150                   : H6_TBID         , at = 0.375        , slot_id = 47534194;
 TCMAA.X0400001                : H6_TCMAA        , at = 0.85         , slot_id = 56992277;
 XVW.X0400001                  : H6_XVWAD001     , at = 1.10095      , slot_id = 57310366;
 MTN.X0400003                  : H6_MTN__HWP     , at = 3.15         , slot_id = 56992202;
 MTN.X0400007                  : H6_MTN__HWP     , at = 7.35         , slot_id = 56992225;
 XVW.X0400017                  : H6_XVWAE001     , at = 16.610987628 , slot_id = 57310461;
 XTAX.X0420018                 : H6_XTAX_005     , at = 18.0075      , slot_id = 57111690;
 XTAX.X0420020                 : H6_XTAX_006     , at = 19.6325      , slot_id = 57111699;
 MSN.X0410022                  : H6_MSN          , at = 22.45        , slot_id = 57381789;
 MSN.X0410029                  : H6_MSN          , at = 29.43        , slot_id = 57382111;
 QSL.X0410033                  : H6_QSL__8WP     , at = 32.92        , slot_id = 57383006;
 QNL.X0410040                  : H6_QNL__8WP     , at = 39.78        , slot_id = 57383363;
 XCSH.X0410043                 : H6_XCSH_001     , at = 42.07        , slot_id = 56051911;
 XCSV.X0410047                 : H6_XCSV_001     , at = 48.015       , slot_id = 57015397;
 QNL.X0410050                  : H6_QNL__8WP     , at = 50.44        , slot_id = 57383399;
 MBNV.X0410055                 : H6_MBNV_HWP     , at = 54.985       , slot_id = 57382147;
 MBNV.X0410061                 : H6_MBNV_HWP     , at = 60.645       , slot_id = 57382428;
 XCSH.X0410064                 : H6_XCSH_001     , at = 64.05        , slot_id = 56051920;
 MBNV.X0410067                 : H6_MBNV_HWP     , at = 67.455       , slot_id = 57382464;
 MBNV.X0410073                 : H6_MBNV_HWP     , at = 73.115       , slot_id = 57382500;
 QNL.X0410078                  : H6_QNL__8WP     , at = 77.66        , slot_id = 57383435;
 QNL.X0410096                  : H6_QNL__8WP     , at = 95.888       , slot_id = 57383471;
 XVW.X0410104                  : H6_XVWAM001     , at = 104.059085964, slot_id = 57601972;
 QTS.X0410106                  : H6_QTS__8WP     , at = 105.334      , slot_id = 56055903;
 MBXHC.X0410117                : H6_MBXHCCWP     , at = 117.42       , slot_id = 56048137;
 MBXHC.X0410121                : H6_MBXHCCWP     , at = 120.72       , slot_id = 56048146;
 MBXHC.X0410124                : H6_MBXHCCWP     , at = 124.02       , slot_id = 56048155;
 XCON.X0410126                 : H6_XCON_009     , at = 126.36       , slot_id = 57585134;
 XCHV.X0410128                 : H6_XCHV_001     , at = 127.665      , slot_id = 56051692;
 LSX.X0410129                  : H6_LSX__FWP     , at = 128.835      , slot_id = 57381715;
 MCXCA.X0410130                : H6_MCXCAHWC     , at = 129.812      , slot_id = 56048223;
 MBXHC.X0410132                : H6_MBXHCCWP     , at = 132.08       , slot_id = 56048196;
 MBXHC.X0410135                : H6_MBXHCCWP     , at = 135.38       , slot_id = 56048205;
 MBXHC.X0410139                : H6_MBXHCCWP     , at = 138.68       , slot_id = 56048214;
 QTS.X0410151                  : H6_QTS__8WP     , at = 150.766      , slot_id = 56055913;
 QNL.X0410160                  : H6_QNL__8WP     , at = 160.212      , slot_id = 57383615;
 QNL.X0410178                  : H6_QNL__8WP     , at = 178.44       , slot_id = 57383687;
 XCHV.X0410192                 : H6_XCHV_001     , at = 192.05       , slot_id = 56051701;
 LSX.X0410193                  : H6_LSX__FWP     , at = 193.215      , slot_id = 57381751;
 MCXCA.X0410194                : H6_MCXCAHWC     , at = 194.197      , slot_id = 56048232;
 MCXCA.X0410195                : H6_MCXCAHWC     , at = 194.967      , slot_id = 56048241;
 QNL.X0410206                  : H6_QNL__8WP     , at = 205.66       , slot_id = 57383723;
 QNL.X0410224                  : H6_QNL__8WP     , at = 223.888      , slot_id = 57383651;
 XWCA.X0410225                 : H6_XWCA         , at = 225.773      , slot_id = 57712204;
 XSCI.X0410225                 : H6_XSCI         , at = 225.853      , slot_id = 57712228;
 XVW.X0410227                  : H6_XVWAM001     , at = 227.063095781, slot_id = 57602261;
 QTS.X0410234                  : H6_QTS__8WP     , at = 233.334      , slot_id = 56055922;
 XFFV.X0410256                 : H6_XFFV         , at = 255.912      , slot_id = 57712251;
 XFFH.X0410257                 : H6_XFFH         , at = 256.188      , slot_id = 57712274;
 XVW.X0410268                  : H6_XVWAL001     , at = 268.330095781, slot_id = 57601954;
 QTS.X0410279                  : H6_QTS__8WP     , at = 278.766      , slot_id = 56055931;
 QNL.X0410288                  : H6_QNL__8WP     , at = 288.212      , slot_id = 57383812;
 XVW.X0410303                  : H6_XVWAM001     , at = 303.875095781, slot_id = 57602288;
 XVW.X0410304                  : H6_XVWAM001     , at = 304.725095781, slot_id = 57602297;
 QNL.X0410306                  : H6_QNL__8WP     , at = 306.44       , slot_id = 57383044;
 MBNV.X0410311                 : H6_MBNV_HWP     , at = 310.985      , slot_id = 57382620;
 MBNV.X0410317                 : H6_MBNV_HWP     , at = 316.645      , slot_id = 57382660;
 XFFV.X0410320                 : H6_XFFV         , at = 319.912      , slot_id = 57712297;
 XFFH.X0410321                 : H6_XFFH         , at = 320.188      , slot_id = 57712320;
 MBNV.X0410323                 : H6_MBNV_HWP     , at = 323.455      , slot_id = 57382696;
 MBNV.X0410329                 : H6_MBNV_HWP     , at = 329.115      , slot_id = 57382732;
 QNL.X0410334                  : H6_QNL__8WP     , at = 333.66       , slot_id = 57383082;
 QNL.X0410352                  : H6_QNL__8WP     , at = 351.888      , slot_id = 57383848;
 XVW.X0410353                  : H6_XVWAM001     , at = 353.703183715, slot_id = 57602306;
 XVW.X0410354                  : H6_XVWAM001     , at = 354.303183715, slot_id = 57602315;
 QTS.X0410362                  : H6_QTS__8WP     , at = 361.334      , slot_id = 56055940;
 XWCA.X0410368                 : H6_XWCA         , at = 367.685      , slot_id = 57712350;
 XSCI.X0410368                 : H6_XSCI         , at = 367.765      , slot_id = 57712379;
 XCON.X0410369                 : H6_XCON_014     , at = 368.19       , slot_id = 57588974;
 XVW.X0410371                  : H6_XVWAM001     , at = 371.580183715, slot_id = 57602333;
 MBNH.X0410375                 : H6_MBNH_HWP     , at = 374.6        , slot_id = 57382768;
 MBNH.X0410380                 : H6_MBNH_HWP     , at = 380.26       , slot_id = 57382806;
 XCSV.X0410384                 : H6_XCSV_001     , at = 383.665      , slot_id = 57015439;
 MCXCA.X0410385                : H6_MCXCAHWC     , at = 384.675      , slot_id = 56048250;
 MBNH.X0410388                 : H6_MBNH_HWP     , at = 387.84       , slot_id = 57382842;
 MBNH.X0410393                 : H6_MBNH_HWP     , at = 393.5        , slot_id = 57382878;
 QTS.X0410397                  : H6_QTS__8WP     , at = 397.265      , slot_id = 56055949;
 XWCA.X0410404                 : H6_XWCA         , at = 405.24       , slot_id = 57712402;
 XCSH.X0410414                 : H6_XCSH_001     , at = 414.38       , slot_id = 56051902;
 QNL.X0410426                  : H6_QNL__8WP     , at = 426.43       , slot_id = 57383177;
 QWL.X0410434                  : H6_QWL__8WP     , at = 434.41       , slot_id = 57383219;
 XFFV.X0410436                 : H6_XFFV         , at = 436.293      , slot_id = 57712425;
 XFFH.X0410437                 : H6_XFFH         , at = 436.569      , slot_id = 57712448;
 XWCA.X0410438                 : H6_XWCA         , at = 436.95       , slot_id = 57712471;
 XSCI.X0410438                 : H6_XSCI         , at = 437.03       , slot_id = 57712688;
 XCED.X0410440                 : H6_XCEDN        , at = 440.5225     , slot_id = 57608988;
 XVW.X0410445                  : H6_XVWAL001     , at = 445.111280915, slot_id = 57601963;
 XCET.X0410449                 : H6_XCET_001     , at = 447.228      , slot_id = 56032605;
 XSCI.X0410450                 : H6_XSCI         , at = 451.105      , slot_id = 57712711;
 XFFH.X0410451                 : H6_XFFH         , at = 451.293      , slot_id = 57713026;
 XFFV.X0410452                 : H6_XFFV         , at = 451.569      , slot_id = 57713003;
 QWL.X0410453                  : H6_QWL__8WP     , at = 453.452      , slot_id = 57383257;
 QNL.X0410461                  : H6_QNL__8WP     , at = 461.432      , slot_id = 57383320;
 MCXCA.X0410473                : H6_MCXCAHWC     , at = 473.8        , slot_id = 56048259;
 MCXCA.X0410474                : H6_MCXCAHWC     , at = 474.56       , slot_id = 56048268;
 XWCA.X0410475                 : H6_XWCA         , at = 475.065      , slot_id = 57713049;
 XSCI.X0410475                 : H6_XSCI         , at = 475.195005   , slot_id = 57713075;
 XEMC.X0410476                 : H6_XEMC_001     , at = 475.88501    , slot_id = 57576715;
 XDWC.X0410488                 : H6_XDWC         , at = 488.22501    , slot_id = 57769019;
 XTDV.X0410528                 : H6_XTDV_001     , at = 527.187      , slot_id = 57612067;
 XWCA.X0410530                 : H6_XWCA         , at = 530.005      , slot_id = 57713121;
 XSCI.X0410530                 : H6_XSCI         , at = 530.085      , slot_id = 57713232;
 XTDV.X0410542                 : H6_XTDV_001     , at = 542.787      , slot_id = 57612103;
ENDSEQUENCE;

//---------------------------- Strength Definitions ----------------------------- 
MTN.X0400003, ANGLE := kB3T, TILT=0.01;
MTN.X0400007, ANGLE := kB3T, TILT=0.01;
MSN.X0410022, ANGLE := kB1, TILT=0.01;
MSN.X0410029, ANGLE := kB2, TILT=0.01;
MBNV.X0410055, ANGLE := kB3, TILT=1.57044726;
MBNV.X0410061, ANGLE := kB3, TILT=1.57044726;
MBNV.X0410067, ANGLE := kB3, TILT=1.57044726;
MBNV.X0410073, ANGLE := kB3, TILT=1.57044726;
MBXHC.X0410117, ANGLE := kB4, TILT=0.01;
MBXHC.X0410121, ANGLE := kB4, TILT=0.01;
MBXHC.X0410124, ANGLE := kB4, TILT=0.01;
MBXHC.X0410132, ANGLE := kB7, TILT=0.01;
MBXHC.X0410135, ANGLE := kB7, TILT=0.01;
MBXHC.X0410139, ANGLE := kB7, TILT=0.01;
MBNV.X0410311, ANGLE := kB5, TILT=1.57044726;
MBNV.X0410317, ANGLE := kB5, TILT=1.57044726;
MBNH.X0410375, ANGLE := kB6, TILT=0.01;
MBNH.X0410380, ANGLE := kB6, TILT=0.01;


return;