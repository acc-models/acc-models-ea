import matplotlib.pyplot as plt
import numpy as np


# S sigma_x  err_sigma_x sigma_y err_sigma_y
profiles = np.array([[2.25800000e+02, 5.39960165e+00, 1.60151148e-01, 3.20008298e+00, 1.51012487e-01],
                     [3.67740000e+02, 2.70693114e+00, 5.68727153e-02, 1.37432634e+00, 7.11752419e-02],
                     [4.05200000e+02, 2.05155359e+00, 8.83141332e-02, 1.43474828e+00, 3.94575828e-02],
                     [4.75100000e+02, 3.88119309e+00, 1.10083556e-01, 1.69789285e+00, 6.75578828e-02],
                     [4.88320000e+02, 1.95123428e+00, 9.46088168e-02, 2.24524455e+00, 4.12259288e-02],
                     [5.29950000e+02, 3.81342700e+00, 9.07973637e-02, 3.40390782e+00, 8.69900958e-02]])



def AddProfilesToSigmaPlot(f):
    axs = f.get_axes()
    ax = axs[0]

    for p in profiles:
        ax.axvline(p[0], c='grey', alpha=0.5)
        ax.errorbar(p[0], p[1]*1e-3, yerr=p[2]*1e-3, fmt=".", c='b') # horizontal
        ax.errorbar(p[0], p[3]*1e-3, yerr=p[4]*1e-3, fmt=".", c='g') # vertical
