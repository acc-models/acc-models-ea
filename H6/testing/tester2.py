import pymadx
a = pymadx.Data.Tfs("h6fm04_Ref_Magnets_all.twiss")
pymadx.Plot.RMatrixOptics2(a, dx=5.0,dy=5.0, dpy=0.1, dpx=0.1,
                           collimatorHRegex=['XCSH+', 'XCHV+'],
                           collimatorVRegex=['XCSV+', 'XCHV+'])
