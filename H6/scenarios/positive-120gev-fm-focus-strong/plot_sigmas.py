import glob
import matplotlib.pyplot as plt
import pymadx


files = glob.glob("focus_ff_*.tfs")

s = []
sx = []
sy = []

for f in sorted(files):
    print(f)
    a = pymadx.Data.Tfs(f)
    pymadx.Plot.Sigma(a)
    plt.xlim(300,560)
    plt.savefig(f[:-4]+"_sigma.pdf")
    s.append(a.GetColumn('S'))
    sx.append(a.GetColumn('SIGMAX'))
    sy.append(a.GetColumn('SIGMAY'))

plt.figure(figsize=(10,5))
for si,sxi,syi in zip(s,sx,sy):
    plt.plot(si,sxi*1e3,c='blue')
    plt.plot(si,syi*1e3,c='green')
plt.xlabel('S (m)')
plt.ylabel('$\sigma_{x,y}$ (mm)')
plt.tight_layout()
pymadx.Plot.AddMachineLatticeToFigure(plt.gcf(), a)
plt.savefig('all_sigmas.pdf')
plt.xlim(250, 560)
