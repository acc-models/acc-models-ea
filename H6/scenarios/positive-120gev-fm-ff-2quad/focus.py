import numpy as _np

from cpymad.madx import Madx
madx = Madx()

import eamagparam
import pymadx
import tabulate

import matplotlib as mpl
mpl.rc('image', cmap='tab20')
import matplotlib.pyplot as plt
#https://matplotlib.org/stable/gallery/color/colormap_reference.html#sphx-glr-gallery-color-colormap-reference-py

strengthNameToType = {'kQ15' : 'QWL.X0410453',
                          'kQ16' : 'QNL.X0410461'}

def GenerateFocusPoints(model="h6-fm-ff-2quad-template.madx",
                        sequence="H6",
                        momentum=120.0,
                        sStart=490.0, sEnd=540.0, nPoints=10):

    markerS = _np.linspace(sStart, sEnd, nPoints)
    
    madx.call(file=model)

    # add relevant markers
    markerNames = {}
    installCommands = []
    for i,s in enumerate(markerS):
        name = "ff_"+str(i)
        madx.input(name+": marker;")
        markerNames[name] = s
        installCommands.append("install, element="+name+", at="+str(s)+";")

    madx.input("seqedit, sequence="+sequence+";")
    for marker in installCommands:
        madx.input(marker)
    madx.input("endedit;")
    madx.input("use, sequence="+sequence+";")

    # try to match the focus at each marker and save output
    quadsToVary = ["kQ15", "kQ16"]
    files = []
    filesS = []
    for marker, s in markerNames.items():
        print(marker, s)
        files.append(FitFocusToMarker(marker, sequence, quadsToVary, momentum, "focus_"))
        filesS.append((files[-1], s))
        
    for f in files:
        a = pymadx.Data.Tfs(f)
        f1,f2 = pymadx.Plot.RMatrixOptics(a, outputfilename=f[:-4]+".pdf")
        plt.close('all')

    currents = []
    s, betx, bety = [], [], []
    fx = plt.figure()
    axx = fx.add_subplot(111)
    fy = plt.figure()
    axy = fy.add_subplot(111)
    for (f,s) in filesS:
        a = pymadx.Data.Tfs(f)
        #rmat = pymadx.Plot._GetRMatrix
        se = a.GetColumn('S')
        betx = a.GetColumn('BETX')
        bety = a.GetColumn('BETY')
        axx.plot(se, betx, label="Focus at " + str(round(s,0))+"m")
        axy.plot(se, bety, label="Focus at " + str(round(s,0))+"m")
        currents.append(eamagparam.programs.tfs2current.LoadTfsAndCalculateCurrents(f))
    axx.axhline(0)
    axy.axhline(0)
    plt.legend()
    axx.set_xlabel('Focus S Location (m)')
    axy.set_xlabel('Focus S Location (m)')
    axx.set_ylabel(r'$\beta_x$ (m)')
    axy.set_ylabel(r'$\beta_y$ (m)')
    fx.set_tight_layout(True)
    fy.set_tight_layout(True)
    axx.set_title('Horizontal')
    axy.set_title('Vertical')
    

    headers = ['Current', "S Focus", *quadsToVary]
    table = []
    c1921 = []
    for f,s,current in zip(files, markerS, currents):
        cd = dict(current)
        row = [f,s, *[cd[strengthNameToType[q]] for q in quadsToVary]]
        c1921.append([cd[strengthNameToType[q]] for q in quadsToVary])
        table.append(row)
    print(tabulate.tabulate(table,
                            headers=headers,
                            tablefmt="grid",
                            floatfmt=".2f"))

    plt.figure()
    c1921 = _np.array(c1921)
    plt.plot(markerS, c1921[:,0], label='kQ15')
    plt.plot(markerS, c1921[:,1], label='kQ16')
    plt.xlabel('Focus Location S (m)')
    plt.ylabel('Current (A)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('focus_currents.pdf')


    #[list(x) for x in zip(*table)],
    

def FitFocusToMarker(marker, sequence, quadsToVary, momentum, prefix):
    m = marker
    s = sequence
    p = momentum
    
    madx.input("match, rmatrix, sequence="+s+", betx=10, alfx=0, bety=10, alfy=0;")
    madx.input("constraint, sequence="+s+", range="+m+", alfx=0;")
    madx.input("constraint, sequence="+s+", range="+m+", alfy=0;")
    madx.input("constraint, sequence="+s+", range="+m+", betx<0.5;")
    madx.input("constraint, sequence="+s+", range="+m+", bety<0.5;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, alfx=0;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, alfy=0;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, betx<750;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, bety<750;")
    for q in quadsToVary:
        fullName = strengthNameToType[q]
        mTypeName = eamagparam.mapping.GetMagnetTypeName(fullName)
        mag = eamagparam.magnet_info.magnets[mTypeName]
        k1Max = mag.bliCurve.K1FromI(mag.bliCurve.maxCurrent, p)
        madx.input("vary, name="+q+", step=1e-3, lower=-"+str(k1Max)+", upper="+str(k1Max)+";")
    #madx.input("jacobian, calls=5000, tolerance=1e-5;")
    madx.input("lmdif, calls=5000, tolerance=1e-10;")
    madx.input("endmatch;")

    fn = prefix+m+".tfs"
    madx.input("twiss, chrom=true, rmatrix=true, betx=10.0, bety=10.0, save, file="+fn+";")
    return fn

    
