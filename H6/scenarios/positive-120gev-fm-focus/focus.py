import numpy as _np

from cpymad.madx import Madx
madx = Madx()

import eamagparam
import pickle
import pymadx
import tabulate

import matplotlib as mpl
mpl.rc('image', cmap='tab20')
import matplotlib.pyplot as plt
#https://matplotlib.org/stable/gallery/color/colormap_reference.html#sphx-glr-gallery-color-colormap-reference-py

strengthNameToType = {'kQ10' : 'QNL.X0410334',
                      'kQ11' : 'QTS.X0410362',
                      'kQ12' : 'QTS.X0410397',
                      'kQ13' : 'QNL.X0410426',
                      'kQ14' : 'QWL.X0410434',
                      'kQ15' : 'QWL.X0410453',
                      'kQ16' : 'QNL.X0410461'}

def GenerateFocusPoints(model="h6-fm-ff-template.madx",
                        sequence="H6",
                        momentum=120.0,
                        sStart=490.0, sEnd=540.0, nPoints=10):

    markerS = _np.linspace(sStart, sEnd, nPoints)
    
    madx.call(file=model)

    # add relevant markers
    markerNames = {}
    installCommands = []
    for i,s in enumerate(markerS):
        name = "ff_"+str(i)
        madx.input(name+": marker;")
        markerNames[name] = s
        installCommands.append("install, element="+name+", at="+str(s)+";")

    madx.input("seqedit, sequence="+sequence+";")
    for marker in installCommands:
        madx.input(marker)
    madx.input("endedit;")
    madx.input("use, sequence="+sequence+";")

    # try to match the focus at each marker and save output
    quadsToVary = list(strengthNameToType.keys())
    files = []
    filesS = []
    for marker, s in markerNames.items():
        print(marker, s)
        files.append(FitFocusToMarker(marker, sequence, quadsToVary, momentum, "focus_"))
        filesS.append((files[-1], s))
        
    for f in files:
        a = pymadx.Data.Tfs(f)
        f1,f2 = pymadx.Plot.RMatrixOptics(a, outputfilename=f[:-4]+".pdf")
        plt.close('all')

    currents = []
    s, betx, bety = [], [], []
    fx = plt.figure()
    axx = fx.add_subplot(111)
    fy = plt.figure()
    axy = fy.add_subplot(111)
    axdx = axx.twinx()
    axdy = axy.twinx()
    for (f,s) in filesS:
        a = pymadx.Data.Tfs(f)
        #rmat = pymadx.Plot._GetRMatrix
        se = a.GetColumn('S')
        betx = a.GetColumn('BETX')
        dx = a.GetColumn('DX')
        bety = a.GetColumn('BETY')
        dy = a.GetColumn('DY')
        axx.plot(se, betx, label="Focus at " + str(round(s,0))+"m")
        axdx.plot(se, dx, '--')
        axy.plot(se, bety, label="Focus at " + str(round(s,0))+"m")
        axdy.plot(se, dy, '--')
        currents.append(eamagparam.programs.tfs2current.LoadTfsAndCalculateCurrents(f))
    #axx.axhline(0)
    #axy.axhline(0)
    plt.legend()
    axx.set_xlabel('Focus S Location (m)')
    axy.set_xlabel('Focus S Location (m)')
    axx.set_ylabel(r'$\beta_x$ (m)')
    axy.set_ylabel(r'$\beta_y$ (m)')
    axdx.set_ylabel(r'$D_x$ (m)')
    axdy.set_ylabel(r'$D_y$ (m)')
    fx.set_tight_layout(True)
    fy.set_tight_layout(True)
    axx.set_title('Horizontal')
    axy.set_title('Vertical')
    axx.set_xlim(300, 560)
    axy.set_xlim(300, 560)
    

    headers = ['Current', "S Focus", *quadsToVary]
    table = []
    c1921 = []
    for f,s,current in zip(files, markerS, currents):
        cd = dict(current)
        row = [f,s, *[cd[strengthNameToType[q]] for q in quadsToVary]]
        c1921.append([cd[strengthNameToType[q]] for q in quadsToVary])
        table.append(row)
    print(tabulate.tabulate(table,
                            headers=headers,
                            tablefmt="grid",
                            floatfmt=".2f"))

    plt.figure()
    c1921 = _np.array(c1921)
    for i,k in enumerate(strengthNameToType.keys()):
        plt.plot(markerS, c1921[:,i], label=k)
    plt.xlabel('Focus Location S (m)')
    plt.ylabel('Current (A)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('focus_currents.pdf')

    f = open("currents-list.dat", "wb")
    pickle.dump(f, (table, headers))
    f.close()


    #[list(x) for x in zip(*table)],
    

def FitFocusToMarker(marker, sequence, quadsToVary, momentum, prefix):
    m = marker
    s = sequence
    p = momentum
    
    madx.input("match, rmatrix, sequence="+s+", betx=10, alfx=0, bety=10, alfy=0;")
    madx.input("constraint, sequence="+s+", range="+m+", alfx=0;")
    madx.input("constraint, sequence="+s+", range="+m+", alfy=0;")
    madx.input("constraint, sequence="+s+", range="+m+", betx<0.5;")
    madx.input("constraint, sequence="+s+", range="+m+", bety<0.5;")
    madx.input("constraint, sequence="+s+", range="+m+", dx=0;")
    madx.input("constraint, sequence="+s+", range="+m+", dy=0;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, alfx=0;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, alfy=0;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, betx<750;")
    madx.input("constraint, sequence="+s+", range=XCED.X0410440, bety<750;")
    madx.input("constraint, sequence="+s+", range=XCSV.X0410384, alfx=0;")
    madx.input("constraint, sequence="+s+", range=XCSV.X0410384, betx<40;")
    madx.input("constraint, sequence="+s+", range=XCSV.X0410384, bety<40;")
    for q in quadsToVary:
        fullName = strengthNameToType[q]
        mTypeName = eamagparam.mapping.GetMagnetTypeName(fullName)
        mag = eamagparam.magnet_info.magnets[mTypeName]
        k1Max = mag.bliCurve.K1FromI(mag.bliCurve.maxCurrent, p)
        madx.input("vary, name="+q+", step=1e-3, lower=-"+str(k1Max)+", upper="+str(k1Max)+";")
    #madx.input("jacobian, calls=5000, tolerance=1e-5;")
    madx.input("lmdif, calls=5000, tolerance=1e-10;")
    madx.input("endmatch;")

    fn = prefix+m+".tfs"
    madx.input("twiss, chrom=true, rmatrix=true, betx=10.0, bety=10.0, save, file="+fn+";")
    return fn

    
