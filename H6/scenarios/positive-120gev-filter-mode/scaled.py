import eamagparam
import numpy as np

#momenta = np.arange(1,21)*10
momenta = [10,60,120,180]

eamagparam.programs.tfs2current.ScaleSet("h6-positive-120gev-fm.tfs",
                                         momenta,
                                         "scaled/",
                                         tablefmt="latex_longtable")
                                         
