from cpymad.madx import Madx
madx = Madx()

import eamagparam
import currents

strengthNameToType = {'kQ10' : 'QNL.X0410306',
                      'kQ11' : 'QTS.X0410362',
                      'kQ12' : 'QTS.X0410397',
                      'kQ13' : 'QNL.X0410426',
                      'kQ14' : 'QWL.X0410434',
                      'kQ15' : 'QWL.X0410453',
                      'kQ16' : 'QNL.X0410461'}

typeToStrengthName = {v:k for k,v in strengthNameToType.items()}

def RunMadx(model="h6-startup.madx", output="real_values.tfs"):

    sequence = "H6",
    momentum = 120.0
    
    madx.call(file=model)

    for magnet,i in currents.currents.items():
        typeName = eamagparam.mapping.GetMagnetTypeName(magnet)
        polarity = eamagparam.mapping.GetPolarity(magnet)
        k1 = eamagparam.I2K1(typeName, i, momentum) * polarity
        sName = typeToStrengthName[magnet]
        print(sName,k1)
        madx.input(sName + " = " + str(k1) + ";")

    madx.input("twiss, chrom=true, rmatrix=true, betx=10.0, bety=10.0, save, file="+output+";")
