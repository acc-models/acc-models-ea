/************************************************************************************************************************************************************************
*
* H6 version YETS 2023-2024 in MAD X SEQUENCE format
* Generated the 08-NOV-2023 02:19:54 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.H6_LSX__FWP                  := 0.7;
l.H6_MBNH_HWP                  := 5;
l.H6_MBNV_HWP                  := 5;
l.H6_MBXHCCWP                  := 2.5;
l.H6_MCXCAHWC                  := 0.4;
l.H6_MSN                       := 3.2;
l.H6_MTN__HWP                  := 3.6;
l.H6_OMK                       := 0;
l.H6_QNL__8WP                  := 2.99;
l.H6_QSL__8WP                  := 3;
l.H6_QTS__8WP                  := 1.49;
l.H6_QWL__8WP                  := 2.948;
l.H6_TBID                      := 0.25;
l.H6_TCMAA                     := 0.4;
l.H6_XCEDN                     := 6.317;
l.H6_XCET_001                  := 0.6;
l.H6_XCHV_001                  := 1;
l.H6_XCON_009                  := 1;
l.H6_XCON_014                  := 0.018;
l.H6_XCSH_001                  := 1;
l.H6_XCSV_001                  := 1;
l.H6_XDWC                      := 0.06;
l.H6_XEMC_001                  := 0.83;
l.H6_XFFH                      := 0.276;
l.H6_XFFV                      := 0.276;
l.H6_XSCI                      := 0.1;
l.H6_XTAX_005                  := 1.615;
l.H6_XTAX_006                  := 1.615;
l.H6_XTDV_001                  := 1.6;
l.H6_XVVSB001                  := 0.32;
l.H6_XVWAA001                  := 0.0001;
l.H6_XVWAB001                  := 0.0001;
l.H6_XVWAE001                  := 0.0002;
l.H6_XVWAL001                  := 0.00012;
l.H6_XVWAM001                  := 0.00012;
l.H6_XWCA                      := 0.06;

//---------------------- COLLIMATOR     ---------------------------------------------
H6_TCMAA       : COLLIMATOR  , L := l.H6_TCMAA;          ! Collimation mask type A
H6_XCHV_001    : COLLIMATOR  , L := l.H6_XCHV_001;       ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
H6_XCON_009    : COLLIMATOR  , L := l.H6_XCON_009;       ! Converter Type 009 - TARGET01 Lead 3 mm and 6 mm, Poly. 1 m, Copper 400 mm
H6_XCON_014    : COLLIMATOR  , L := l.H6_XCON_014;       ! Converter Type 014 - Lead Absorber 4 mm, 8 mm, 18 mm
H6_XCSH_001    : COLLIMATOR  , L := l.H6_XCSH_001;       ! SPS Collimator a fente horizontal (design 1970)
H6_XCSV_001    : COLLIMATOR  , L := l.H6_XCSV_001;       ! SPS Collimator a fente vertical (design 1970)
H6_XTAX_005    : COLLIMATOR  , L := l.H6_XTAX_005;       ! Target Absorber Type 005
H6_XTAX_006    : COLLIMATOR  , L := l.H6_XTAX_006;       ! Target Absorber Type 006
H6_XTDV_001    : COLLIMATOR  , L := l.H6_XTDV_001;       ! Mobile Dump Vertical (Hydraulic)
//---------------------- HKICKER        ---------------------------------------------
H6_MSN         : HKICKER     , L := l.H6_MSN;            ! Septum magnet, north area - offset mechanical and optical dimensions according to drawing 1766138 (not well indicated)
//---------------------- INSTRUMENT     ---------------------------------------------
H6_XCEDN       : INSTRUMENT  , L := l.H6_XCEDN;          ! Cherenkov Differential Counter Nord
H6_XCET_001    : INSTRUMENT  , L := l.H6_XCET_001;       ! Cherenkov Counter DN159 - Mylar Window
H6_XEMC_001    : INSTRUMENT  , L := l.H6_XEMC_001;       ! ElectroMagnetic Calorimeter
H6_XVVSB001    : INSTRUMENT  , L := l.H6_XVVSB001;       ! X Vacuum Sector ElectroValve DN159x320mm (local control) - VAT
H6_XVWAA001    : INSTRUMENT  , L := l.H6_XVWAA001;       ! X Vacuum Window Aluminium + pumping port DN40 DN120x0.1
H6_XVWAB001    : INSTRUMENT  , L := l.H6_XVWAB001;       ! X Vacuum Window Aluminium EL350x60x0.1
H6_XVWAE001    : INSTRUMENT  , L := l.H6_XVWAE001;       ! X Vacuum Window Aluminium EL900X60X0.2
H6_XVWAL001    : INSTRUMENT  , L := l.H6_XVWAL001;       ! X Vacuum Window Mylar DN120x70x0.12
H6_XVWAM001    : INSTRUMENT  , L := l.H6_XVWAM001;       ! X Vacuum Window Mylar + pumping port DN40 DN120x100x0.12
//---------------------- KICKER         ---------------------------------------------
H6_MCXCAHWC    : KICKER      , L := l.H6_MCXCAHWC;       ! Corrector magnet, H or V, type MDX
//---------------------- MARKER         ---------------------------------------------
H6_OMK         : MARKER      , L := l.H6_OMK;            ! H6 markers
//---------------------- MONITOR        ---------------------------------------------
H6_TBID        : MONITOR     , L := l.H6_TBID;           ! target beam instrumentation, downstream
H6_XDWC        : MONITOR     , L := l.H6_XDWC;           ! Delay Wire Chamber (Beam Profile Monitors)
H6_XFFH        : MONITOR     , L := l.H6_XFFH;           ! Filament Scintillator Profile Monitor - Horizontal
H6_XFFV        : MONITOR     , L := l.H6_XFFV;           ! Filament Scintillator Profile Monitor - Vertical
H6_XSCI        : MONITOR     , L := l.H6_XSCI;           ! Ensemble: Cadre et Scintillateur BXSCI
H6_XWCA        : MONITOR     , L := l.H6_XWCA;           ! Multi Wire Proportional Chamber Assembly
//---------------------- QUADRUPOLE     ---------------------------------------------
H6_QNL__8WP    : QUADRUPOLE  , L := l.H6_QNL__8WP;       ! Quadrupole, secondary beams, type north area
H6_QSL__8WP    : QUADRUPOLE  , L := l.H6_QSL__8WP;       ! Quadrupole, slim, long, - Same magnet type SPQSLD_
H6_QTS__8WP    : QUADRUPOLE  , L := l.H6_QTS__8WP;       ! Quadrupole, BT line, short - Same magnet type as SPQTSD_
H6_QWL__8WP    : QUADRUPOLE  , L := l.H6_QWL__8WP;       ! Quadrupole, Secondary Beams, West Area Type
//---------------------- RBEND          ---------------------------------------------
H6_MBNH_HWP    : RBEND       , L := l.H6_MBNH_HWP;       ! Bending magnet, secondary beams, horizontal, north area
H6_MBNV_HWP    : RBEND       , L := l.H6_MBNV_HWP;       ! Bending magnet, secondary beams, vertical, north area - Mechanical dimensions from NORMA
H6_MBXHCCWP    : RBEND       , L := l.H6_MBXHCCWP;       ! Bending Magnet, H or V, type HB1, 2.5m gap 80mm
H6_MTN__HWP    : RBEND       , L := l.H6_MTN__HWP;       ! Bending magnet, Target N
//---------------------- SEXTUPOLE      ---------------------------------------------
H6_LSX__FWP    : SEXTUPOLE   , L := l.H6_LSX__FWP;       ! Sextupole lens, north area

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kMBH.X0400003_BEND3T           := -0.0126;
kMBH.X0400007_BEND3T           := -0.0126;
kMBH.X0410022_BEND1H           := -0.0056;
kMBH.X0410029_BEND2H           := -0.007;
kMBH.X0410117_BEND4H           := 0.0039634;
kMBH.X0410121_BEND4H           := 0.0039634;
kMBH.X0410124_BEND4H           := 0.0039634;
kMBH.X0410132_BEND7H           := 0.0039634;
kMBH.X0410135_BEND7H           := 0.0039634;
kMBH.X0410139_BEND7H           := 0.0039634;
kMBH.X0410375_BEND6H           := -0.0108;
kMBH.X0410380_BEND6H           := -0.0108;
kMBH.X0410388_BEND6H           := -0.0108;
kMBH.X0410393_BEND6H           := -0.0108;
kMBV.X0410055_BEND3V           := -0.010254;
kMBV.X0410067_BEND3V           := -0.010254;
kMBV.X0410073_BEND3V           := -0.010254;
kMBV.X0410311_BEND5V           := 0.0102723;
kMBV.X0410317_BEND5V           := 0.0102723;
kMBV.X0410323_BEND5V           := 0.0102723;
kMBV.X0410329_BEND5V           := 0.0102723;
tilt.MBH.X0400003_BEND3T       := -0.0002617993877991494;
tilt.MBH.X0400007_BEND3T       := -0.0002663560492204833;
tilt.MBH.X0410022_BEND1H       := -0.00027095391164916327;
tilt.MBH.X0410029_BEND2H       := -0.00027301062849510293;
tilt.MBH.X0410117_BEND4H       := -0.00027537668982465153;
tilt.MBH.X0410121_BEND4H       := -0.0004365641492769975;
tilt.MBH.X0410124_BEND4H       := -0.0005977544599117669;
tilt.MBH.X0410132_BEND7H       := -0.0007589476217043422;
tilt.MBH.X0410135_BEND7H       := -0.0009201436346087759;
tilt.MBH.X0410139_BEND7H       := -0.0010813424986270631;
tilt.MBH.X0410375_BEND6H       := -0.001242760525258184;
tilt.MBH.X0410380_BEND6H       := -0.0012475138189680065;
tilt.MBH.X0410388_BEND6H       := -0.0012522973714467393;
tilt.MBH.X0410393_BEND6H       := -0.0012571111809220304;
tilt.MBV.X0410055_BEND3V       := 1.5705207337729163;
tilt.MBV.X0410067_BEND3V       := 1.5705207868478794;
tilt.MBV.X0410073_BEND3V       := 1.570520854695833;
tilt.MBV.X0410311_BEND5V       := 1.56955378258116;
tilt.MBV.X0410317_BEND5V       := 1.5695536870495257;
tilt.MBV.X0410323_BEND5V       := 1.5695536191603665;
tilt.MBV.X0410329_BEND5V       := 1.5695535789022648;
tilt.MCBH.X0410195_TRIM3H      := -0.0012425442137363977;
tilt.MCBH.X0410474_TRIM6H      := -0.0012619552456373821;
tilt.MCBV.X0410130_TRIM1V      := 1.5700373791731923;
tilt.MCBV.X0410194_TRIM2V      := 1.56955378258116;
tilt.MCBV.X0410385_TRIM4V      := 1.5695440294234497;
tilt.MCBV.X0410473_TRIM5V      := 1.5695343715492591;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 MSL.X0410129_6P1                                  : H6_LSX__FWP     , APERTYPE=ELLIPSE, APERTURE={.076,.01915,.076,.01915}, K2 := kMSL.X0410129_6P1;
 MSL.X0410193_6P2                                  : H6_LSX__FWP     , APERTYPE=ELLIPSE, APERTURE={.076,.01915,.076,.01915}, K2 := kMSL.X0410193_6P2;
 MBH.X0410375_BEND6H                               : H6_MBNH_HWP     , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBH.X0410375_BEND6H, TILT := tilt.MBH.X0410375_BEND6H;
 MBH.X0410380_BEND6H                               : H6_MBNH_HWP     , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBH.X0410380_BEND6H, TILT := tilt.MBH.X0410380_BEND6H;
 MBH.X0410388_BEND6H                               : H6_MBNH_HWP     , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBH.X0410388_BEND6H, TILT := tilt.MBH.X0410388_BEND6H;
 MBH.X0410393_BEND6H                               : H6_MBNH_HWP     , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBH.X0410393_BEND6H, TILT := tilt.MBH.X0410393_BEND6H;
 MBV.X0410055_BEND3V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410055_BEND3V, TILT := tilt.MBV.X0410055_BEND3V;
 MBV.X0410061_BEND3V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125};
 MBV.X0410067_BEND3V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410067_BEND3V, TILT := tilt.MBV.X0410067_BEND3V;
 MBV.X0410073_BEND3V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410073_BEND3V, TILT := tilt.MBV.X0410073_BEND3V;
 MBV.X0410311_BEND5V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410311_BEND5V, TILT := tilt.MBV.X0410311_BEND5V;
 MBV.X0410317_BEND5V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410317_BEND5V, TILT := tilt.MBV.X0410317_BEND5V;
 MBV.X0410323_BEND5V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410323_BEND5V, TILT := tilt.MBV.X0410323_BEND5V;
 MBV.X0410329_BEND5V                               : H6_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0410329_BEND5V, TILT := tilt.MBV.X0410329_BEND5V;
 MBH.X0410117_BEND4H                               : H6_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0410117_BEND4H, TILT := tilt.MBH.X0410117_BEND4H;
 MBH.X0410121_BEND4H                               : H6_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0410121_BEND4H, TILT := tilt.MBH.X0410121_BEND4H;
 MBH.X0410124_BEND4H                               : H6_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0410124_BEND4H, TILT := tilt.MBH.X0410124_BEND4H;
 MBH.X0410132_BEND7H                               : H6_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0410132_BEND7H, TILT := tilt.MBH.X0410132_BEND7H;
 MBH.X0410135_BEND7H                               : H6_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0410135_BEND7H, TILT := tilt.MBH.X0410135_BEND7H;
 MBH.X0410139_BEND7H                               : H6_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0410139_BEND7H, TILT := tilt.MBH.X0410139_BEND7H;
 MCBV.X0410130_TRIM1V                              : H6_MCXCAHWC     , TILT := tilt.MCBV.X0410130_TRIM1V, KICK := kMCBV.X0410130_TRIM1V;
 MCBV.X0410194_TRIM2V                              : H6_MCXCAHWC     , TILT := tilt.MCBV.X0410194_TRIM2V, KICK := kMCBV.X0410194_TRIM2V;
 MCBH.X0410195_TRIM3H                              : H6_MCXCAHWC     , TILT := tilt.MCBH.X0410195_TRIM3H, KICK := kMCBH.X0410195_TRIM3H;
 MCBV.X0410385_TRIM4V                              : H6_MCXCAHWC     , TILT := tilt.MCBV.X0410385_TRIM4V, KICK := kMCBV.X0410385_TRIM4V;
 MCBV.X0410473_TRIM5V                              : H6_MCXCAHWC     , TILT := tilt.MCBV.X0410473_TRIM5V, KICK := kMCBV.X0410473_TRIM5V;
 MCBH.X0410474_TRIM6H                              : H6_MCXCAHWC     , TILT := tilt.MCBH.X0410474_TRIM6H, KICK := kMCBH.X0410474_TRIM6H;
 MBH.X0410022_BEND1H                               : H6_MSN          , APERTYPE=RACETRACK, APERTURE={.57,.03,.57,.03}, ANGLE := kMBH.X0410022_BEND1H, TILT := tilt.MBH.X0410022_BEND1H;
 MBH.X0410029_BEND2H                               : H6_MSN          , APERTYPE=RACETRACK, APERTURE={.57,.03,.57,.03}, ANGLE := kMBH.X0410029_BEND2H, TILT := tilt.MBH.X0410029_BEND2H;
 MBH.X0400003_BEND3T                               : H6_MTN__HWP     , APERTYPE=RECTANGLE, APERTURE={.175,.03,.175,.03}, ANGLE := kMBH.X0400003_BEND3T, TILT := tilt.MBH.X0400003_BEND3T;
 MBH.X0400007_BEND3T                               : H6_MTN__HWP     , APERTYPE=RECTANGLE, APERTURE={.175,.03,.175,.03}, ANGLE := kMBH.X0400007_BEND3T, TILT := tilt.MBH.X0400007_BEND3T;
 MQD.X0410040_QUAD2D                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410040_QUAD2D;
 MQD.X0410050_QUAD3D                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04};
 MQD.X0410078_QUAD4D                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410078_QUAD4D;
 MQF.X0410096_QUAD5F                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0410096_QUAD5F;
 MQF.X0410160_QUAD7F                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0410160_QUAD7F;
 MQD.X0410178_QUAD8D                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410178_QUAD8D;
 MQD.X0410206_QUAD8D                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410206_QUAD8D;
 MQF.X0410224_QUAD7F                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0410224_QUAD7F;
 MQF.X0410288_QUAD9F                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0410288_QUAD9F;
 MQD.X0410306_QUAD10D                              : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410306_QUAD10D;
 MQD.X0410334_QUAD10D                              : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410334_QUAD10D;
 MQF.X0410352_QUAD9F                               : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0410352_QUAD9F;
 MQD.X0410426_QUAD13D                              : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410426_QUAD13D;
 MQD.X0410461_QUAD16D                              : H6_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410461_QUAD16D;
 MQF.X0410033_QUAD1F                               : H6_QSL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQF.X0410033_QUAD1F;
 MQD.X0410106_QUAD6D                               : H6_QTS__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410106_QUAD6D;
 MQD.X0410151_QUAD6D                               : H6_QTS__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410151_QUAD6D;
 MQD.X0410234_QUAD6D                               : H6_QTS__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410234_QUAD6D;
 MQD.X0410279_QUAD6D                               : H6_QTS__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410279_QUAD6D;
 MQD.X0410362_QUAD11D                              : H6_QTS__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0410362_QUAD11D;
 MQF.X0410397_QUAD12F                              : H6_QTS__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0410397_QUAD12F;
 MQF.X0410434_QUAD14F                              : H6_QWL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQF.X0410434_QUAD14F;
 MQF.X0410453_QUAD15F                              : H6_QWL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQF.X0410453_QUAD15F;
 TARGET01                                          : H6_XCON_009;
 XVSV.041.104                                      : H6_XVVSB001     , APERTYPE=CIRCLE, APERTURE={.1305,.1305,.1305,.1305};
 VXWT                                              : H6_XVWAB001     , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXWT                                              : H6_XVWAE001     , APERTYPE=CIRCLE, APERTURE={.45,.45,.45,.45};

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

H6 : SEQUENCE, refer = centre,          L = 555.197;
 TBACA.X0400000                : H6_OMK                      , at = 0            , slot_id = 56964667;
 TBID.241150                   : H6_TBID                     , at = .325         , slot_id = 47534194;
 TCMAA.X0400001                : H6_TCMAA                    , at = .85          , slot_id = 56992277;
 XVW.X0400001                  : VXWT                        , at = 1.126        , slot_id = 57310366;
 MTN.X0400003                  : MBH.X0400003_BEND3T         , at = 3.15         , slot_id = 56992202;
 MTN.X0400007                  : MBH.X0400007_BEND3T         , at = 5.55         , slot_id = 56992225;
 XVW.X0400017                  : VXWT                        , at = 16.610987628 , slot_id = 57310461;
 XTAX.X0420018                 : H6_XTAX_005                 , at = 18.0075      , slot_id = 57111690;
 XTAX.X0420020                 : H6_XTAX_006                 , at = 19.6325      , slot_id = 57111699;
 MSN.X0410022                  : MBH.X0410022_BEND1H         , at = 22.45        , slot_id = 57381789;
 MSN.X0410029                  : MBH.X0410029_BEND2H         , at = 29.43        , slot_id = 57382111;
 QSL.X0410033                  : MQF.X0410033_QUAD1F         , at = 32.92        , slot_id = 57383006;
 QNL.X0410040                  : MQD.X0410040_QUAD2D         , at = 39.78        , slot_id = 57383363;
 XCSH.X0410043                 : H6_XCSH_001                 , at = 42.07        , slot_id = 56051911;
 XCSV.X0410047                 : H6_XCSV_001                 , at = 48.015       , slot_id = 57015397;
 QNL.X0410050                  : MQD.X0410050_QUAD3D         , at = 50.44        , slot_id = 57383399;
 MBNV.X0410055                 : MBV.X0410055_BEND3V         , at = 54.985       , slot_id = 57382147;
 MBNV.X0410061                 : MBV.X0410061_BEND3V         , at = 60.645       , slot_id = 57382428;
 XCSH.X0410064                 : H6_XCSH_001                 , at = 64.05        , slot_id = 56051920;
 MBNV.X0410067                 : MBV.X0410067_BEND3V         , at = 67.455       , slot_id = 57382464;
 MBNV.X0410073                 : MBV.X0410073_BEND3V         , at = 73.115       , slot_id = 57382500;
 QNL.X0410078                  : MQD.X0410078_QUAD4D         , at = 77.66        , slot_id = 57383435;
 QNL.X0410096                  : MQF.X0410096_QUAD5F         , at = 95.888       , slot_id = 57383471;
 XVVS.X0410104                 : XVSV.041.104                , at = 104          , slot_id = 57596751;
 XVW.X0410104                  : H6_XVWAM001                 , at = 104.059085964, slot_id = 57601972;
 XVW.X0410105                  : H6_XVWAM001                 , at = 104.339085964, slot_id = 57601981;
 QTS.X0410106                  : MQD.X0410106_QUAD6D         , at = 105.334      , slot_id = 56055903;
 MBXHC.X0410117                : MBH.X0410117_BEND4H         , at = 117.42       , slot_id = 56048137;
 MBXHC.X0410121                : MBH.X0410121_BEND4H         , at = 120.72       , slot_id = 56048146;
 MBXHC.X0410124                : MBH.X0410124_BEND4H         , at = 124.02       , slot_id = 56048155;
 XVW.X0410125                  : H6_XVWAA001                 , at = 125.740100873, slot_id = 57601927;
 XCON.X0410126                 : TARGET01                    , at = 126.36       , slot_id = 57585134;
 XVW.X0410126                  : H6_XVWAA001                 , at = 126.990100873, slot_id = 57601936;
 XCHV.X0410128                 : H6_XCHV_001                 , at = 127.665      , slot_id = 56051692;
 LSX.X0410129                  : MSL.X0410129_6P1            , at = 128.835      , slot_id = 57381715;
 MCXCA.X0410130                : MCBV.X0410130_TRIM1V        , at = 129.812      , slot_id = 56048223;
 MBXHC.X0410132                : MBH.X0410132_BEND7H         , at = 132.08       , slot_id = 56048196;
 MBXHC.X0410135                : MBH.X0410135_BEND7H         , at = 135.38       , slot_id = 56048205;
 MBXHC.X0410139                : MBH.X0410139_BEND7H         , at = 138.68       , slot_id = 56048214;
 XVW.X0410143                  : H6_XVWAM001                 , at = 143.360095781, slot_id = 57602216;
 XVW.X0410144                  : H6_XVWAM001                 , at = 144.360095781, slot_id = 57602225;
 QTS.X0410151                  : MQD.X0410151_QUAD6D         , at = 150.766      , slot_id = 56055913;
 QNL.X0410160                  : MQF.X0410160_QUAD7F         , at = 160.212      , slot_id = 57383615;
 QNL.X0410178                  : MQD.X0410178_QUAD8D         , at = 178.44       , slot_id = 57383687;
 XVW.X0410190                  : H6_XVWAM001                 , at = 190.356095781, slot_id = 57602234;
 XVW.X0410191                  : H6_XVWAM001                 , at = 191.475095781, slot_id = 57602243;
 XCHV.X0410192                 : H6_XCHV_001                 , at = 192.05       , slot_id = 56051701;
 LSX.X0410193                  : MSL.X0410193_6P2            , at = 193.215      , slot_id = 57381751;
 MCXCA.X0410194                : MCBV.X0410194_TRIM2V        , at = 194.197      , slot_id = 56048232;
 MCXCA.X0410195                : MCBH.X0410195_TRIM3H        , at = 194.967      , slot_id = 56048241;
 QNL.X0410206                  : MQD.X0410206_QUAD8D         , at = 205.66       , slot_id = 57383723;
 QNL.X0410224                  : MQF.X0410224_QUAD7F         , at = 223.888      , slot_id = 57383651;
 XVW.X0410225                  : H6_XVWAM001                 , at = 225.673095781, slot_id = 57602252;
 XWCA.X0410225                 : H6_XWCA                     , at = 225.773      , slot_id = 57712204;
 XSCI.X0410225                 : H6_XSCI                     , at = 225.853      , slot_id = 57712228;
 XVW.X0410227                  : H6_XVWAM001                 , at = 227.063095781, slot_id = 57602261;
 QTS.X0410234                  : MQD.X0410234_QUAD6D         , at = 233.334      , slot_id = 56055922;
 XVW.X0410238                  : H6_XVWAM001                 , at = 238          , slot_id = 57602270;
 XVW.X0410239                  : H6_XVWAM001                 , at = 239          , slot_id = 57602279;
 XVW.X0410243                  : H6_XVWAL001                 , at = 243.870095781, slot_id = 57601945;
 XFFV.X0410256                 : H6_XFFV                     , at = 255.912      , slot_id = 57712251;
 XFFH.X0410257                 : H6_XFFH                     , at = 256.188      , slot_id = 57712274;
 XVW.X0410268                  : H6_XVWAL001                 , at = 268.330095781, slot_id = 57601954;
 QTS.X0410279                  : MQD.X0410279_QUAD6D         , at = 278.766      , slot_id = 56055931;
 QNL.X0410288                  : MQF.X0410288_QUAD9F         , at = 288.212      , slot_id = 57383812;
 XVW.X0410303                  : H6_XVWAM001                 , at = 303.875095781, slot_id = 57602288;
 XVW.X0410304                  : H6_XVWAM001                 , at = 304.725095781, slot_id = 57602297;
 QNL.X0410306                  : MQD.X0410306_QUAD10D        , at = 306.44       , slot_id = 57383044;
 MBNV.X0410311                 : MBV.X0410311_BEND5V         , at = 310.985      , slot_id = 57382620;
 MBNV.X0410317                 : MBV.X0410317_BEND5V         , at = 316.645      , slot_id = 57382660;
 XFFV.X0410320                 : H6_XFFV                     , at = 319.912      , slot_id = 57712297;
 XFFH.X0410321                 : H6_XFFH                     , at = 320.188      , slot_id = 57712320;
 MBNV.X0410323                 : MBV.X0410323_BEND5V         , at = 323.455      , slot_id = 57382696;
 MBNV.X0410329                 : MBV.X0410329_BEND5V         , at = 329.115      , slot_id = 57382732;
 QNL.X0410334                  : MQD.X0410334_QUAD10D        , at = 333.66       , slot_id = 57383082;
 QNL.X0410352                  : MQF.X0410352_QUAD9F         , at = 351.888      , slot_id = 57383848;
 XVW.X0410353                  : H6_XVWAM001                 , at = 353.703183715, slot_id = 57602306;
 XVW.X0410354                  : H6_XVWAM001                 , at = 354.303183715, slot_id = 57602315;
 QTS.X0410362                  : MQD.X0410362_QUAD11D        , at = 361.334      , slot_id = 56055940;
 XVW.X0410366                  : H6_XVWAM001                 , at = 366.680183715, slot_id = 57602324;
 XWCA.X0410368                 : H6_XWCA                     , at = 367.685      , slot_id = 57712350;
 XSCI.X0410368                 : H6_XSCI                     , at = 367.765      , slot_id = 57712379;
 XCON.X0410369                 : H6_XCON_014                 , at = 368.19       , slot_id = 57588974;
 XVW.X0410371                  : H6_XVWAM001                 , at = 371.580183715, slot_id = 57602333;
 MBNH.X0410375                 : MBH.X0410375_BEND6H         , at = 374.6        , slot_id = 57382768;
 MBNH.X0410380                 : MBH.X0410380_BEND6H         , at = 380.26       , slot_id = 57382806;
 XCSV.X0410384                 : H6_XCSV_001                 , at = 383.665      , slot_id = 57015439;
 MCXCA.X0410385                : MCBV.X0410385_TRIM4V        , at = 384.675      , slot_id = 56048250;
 MBNH.X0410388                 : MBH.X0410388_BEND6H         , at = 387.84       , slot_id = 57382842;
 MBNH.X0410393                 : MBH.X0410393_BEND6H         , at = 393.5        , slot_id = 57382878;
 QTS.X0410397                  : MQF.X0410397_QUAD12F        , at = 397.265      , slot_id = 56055949;
 XVW.X0410403                  : H6_XVWAM001                 , at = 403          , slot_id = 57602342;
 XVW.X0410404                  : H6_XVWAM001                 , at = 404          , slot_id = 57602351;
 XWCA.X0410404                 : H6_XWCA                     , at = 405.24       , slot_id = 57712402;
 XCSH.X0410414                 : H6_XCSH_001                 , at = 414.38       , slot_id = 56051902;
 QNL.X0410426                  : MQD.X0410426_QUAD13D        , at = 426.43       , slot_id = 57383177;
 QWL.X0410434                  : MQF.X0410434_QUAD14F        , at = 434.41       , slot_id = 57383219;
 XFFV.X0410436                 : H6_XFFV                     , at = 436.293      , slot_id = 57712425;
 XFFH.X0410437                 : H6_XFFH                     , at = 436.569      , slot_id = 57712448;
 XWCA.X0410438                 : H6_XWCA                     , at = 436.95       , slot_id = 57712471;
 XSCI.X0410438                 : H6_XSCI                     , at = 437.03       , slot_id = 57712688;
 XCED.X0410440                 : H6_XCEDN                    , at = 440.5225     , slot_id = 57608988;
 XVW.X0410443                  : H6_XVWAM001                 , at = 443          , slot_id = 57602360;
 XVW.X0410445                  : H6_XVWAL001                 , at = 445.111280915, slot_id = 57601963;
 XCET.X0410449                 : H6_XCET_001                 , at = 447.228      , slot_id = 56032605;
 XSCI.X0410450                 : H6_XSCI                     , at = 451.105      , slot_id = 57712711;
 XFFH.X0410452                 : H6_XFFH                     , at = 451.293      , slot_id = 57713026;
 XFFV.X0410451                 : H6_XFFV                     , at = 451.293      , slot_id = 57713003;
 QWL.X0410453                  : MQF.X0410453_QUAD15F        , at = 453.452      , slot_id = 57383257;
 QNL.X0410461                  : MQD.X0410461_QUAD16D        , at = 461.432      , slot_id = 57383320;
 MCXCA.X0410473                : MCBV.X0410473_TRIM5V        , at = 473.8        , slot_id = 56048259;
 MCXCA.X0410474                : MCBH.X0410474_TRIM6H        , at = 474.56       , slot_id = 56048268;
 XVW.X0410475                  : H6_XVWAM001                 , at = 475          , slot_id = 57602369;
 XWCA.X0410475                 : H6_XWCA                     , at = 475.065      , slot_id = 57713049;
 XSCI.X0410475                 : H6_XSCI                     , at = 475.195005   , slot_id = 57713075;
 XEMC.X0410476                 : H6_XEMC_001                 , at = 475.88501    , slot_id = 57576715;
 XVW.X0410476                  : H6_XVWAM001                 , at = 476          , slot_id = 57602378;
 XVW.X0410483                  : H6_XVWAM001                 , at = 483          , slot_id = 57602387;
 XVW.X0410488                  : H6_XVWAM001                 , at = 488          , slot_id = 57602396;
 XDWC.X0410488                 : H6_XDWC                     , at = 488.22501    , slot_id = 57769019;
 XSCI.X0410488                 : H6_XSCI                     , at = 488.42001    , slot_id = 57713098;
 XVW.X0410489                  : H6_XVWAM001                 , at = 489          , slot_id = 57602405;
 XVW.X0410495                  : H6_XVWAM001                 , at = 495          , slot_id = 57602414;
 XVW.X0410513                  : H6_XVWAM001                 , at = 513          , slot_id = 57602423;
 XTDV.X0410528                 : H6_XTDV_001                 , at = 526.377      , slot_id = 57612067;
 XTDV.X0410529                 : H6_XTDV_001                 , at = 527.997      , slot_id = 58239157;
 XVW.X0410528                  : H6_XVWAM001                 , at = 528          , slot_id = 57602432;
 XWCA.X0410530                 : H6_XWCA                     , at = 530.005      , slot_id = 57713121;
 XSCI.X0410530                 : H6_XSCI                     , at = 530.085      , slot_id = 57713232;
 XTDV.X0410542                 : H6_XTDV_001                 , at = 541.977      , slot_id = 57612103;
 XTDV.X0410543                 : H6_XTDV_001                 , at = 543.597      , slot_id = 58239193;
ENDSEQUENCE;

return;