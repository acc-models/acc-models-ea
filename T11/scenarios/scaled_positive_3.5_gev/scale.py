import eamagparam
import eamagparam.programs.tfs2current as t2c
import numpy as np
import tabulate


def ScaleRange(baseTfsFile, pStart, pEnd, nPoints, outputPrefix):
    momenta = np.linspace(pStart, pEnd, nPoints)
    ScaleSet(baseTfsFile, momenta)


def ScaleSet(baseTfsFile, momenta, outputPrefix):
    scenarios = []
    for p in momenta:
        currents = t2c.LoadTfsAndCalculateCurrents(baseTfsFile, p)
        scenarios.append(dict(currents))
        t2c.WriteCurrents(currents, outputPrefix+str(p)+"_gev.dat")

    table = []
    headers = []
    for i,s in enumerate(scenarios):
        if i == 0:
            table.append(list(s.keys()))
            headers.append("Currents")
        table.append(list(s.values()))
        headers.append(str(momenta[i])+" GeV/c")

    # this list comprehension transposes the 2D list (magic)
    print(tabulate.tabulate([list(x) for x in zip(*table)],
                            headers=headers,
                            tablefmt="grid",
                            floatfmt=".2f"))
                     


if __name__ == "__main__":
    momenta = [0.1, 0.3, 0.5, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]
    ScaleSet("t11_p3.5_gev_twiss.tfs",
             momenta,
             "scaled_")
