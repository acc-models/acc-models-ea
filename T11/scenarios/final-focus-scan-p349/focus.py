import numpy as _np

from cpymad.madx import Madx
madx = Madx()

import eamagparam
import pymadx
import tabulate

import matplotlib as mpl
mpl.rc('image', cmap='tab20')
import matplotlib.pyplot as plt
#https://matplotlib.org/stable/gallery/color/colormap_reference.html#sphx-glr-gallery-color-colormap-reference-py

# T11.XBPF022 : T11.XBPF022  , at = 21.9246 , slot_id = 53534888;
# T11.EXP027  : T11.EXP01    , at = 27.0005 , slot_id = 57435104;

# nominal focus ~22.3m

def InterleaveMarkers(madxInstance, sequenceName, positions, labels):
    m = madxInstance
    seq = m.sequence[sequenceName]
    n_exist = seq.element_names()
    s_exist = seq.element_positions()

    i_current = 0
    s_current = positions[i_current]

def GenerateFocusPoints(sStart=23.0, sEnd=39.0, nPoints=17):

    markerS = _np.linspace(sStart, sEnd, nPoints)
    
    madx.call(file="positive_3.5_gev.madx")

    # add relevant markers
    markerNames = {}
    installCommands = []
    for i,s in enumerate(markerS):
        name = "fm_"+str(i)
        madx.input(name+": marker;")
        markerNames[name] = s
        installCommands.append("install, element="+name+", at="+str(s)+";")

    madx.input("seqedit, sequence=T11;")
    for marker in installCommands:
        madx.input(marker)
    madx.input("endedit;")
    madx.input("use, sequence=T11;")

    # try to match the focus at each marker and save output
    quadsToVary = ["T11.MQNEV019", "T11.MQNEV021"]
    files = []
    for marker, s in markerNames.items():
        files.append(FitFocusToMarker(marker, s, quadsToVary, 3.5, "focus_"))
        
    for f in files:
        a = pymadx.Data.Tfs(f)
        pymadx.Plot.RMatrixOptics(a, outputfilename=f[:-4]+".pdf")

    currents = []
    s, r12, r34 = [], [], []
    fx = plt.figure()
    axx = fx.add_subplot(111)
    fy = plt.figure()
    axy = fy.add_subplot(111)
    for f in files:
        a = pymadx.Data.Tfs(f)
        #rmat = pymadx.Plot._GetRMatrix
        se = a.GetColumn('S')
        re12 = a.GetColumn('RE12')
        re34 = a.GetColumn('RE34')
        axx.plot(se, re12, label=f+" R23")
        axy.plot(se, re34, label=f+" R34")
        currents.append(eamagparam.programs.tfs2current.LoadTfsAndCalculateCurrents(f))
    axx.axhline(0)
    axy.axhline(0)
    plt.legend()

    quadsToVaryPlusAmps = [q+" (A)" for q in quadsToVary]
    headers = ['File Name', "S Focus", *quadsToVaryPlusAmps]
    table = []
    c1921 = []
    for f,s,current in zip(files, markerS, currents):
        cd = dict(current)
        row = [f,s, *[cd[q] for q in quadsToVary]]
        c1921.append([cd[q] for q in quadsToVary])
        table.append(row)
    print(tabulate.tabulate(table,
                            headers=headers,
                            tablefmt="grid",
                            floatfmt=".2f"))

    plt.figure()
    c1921 = _np.array(c1921)
    plt.plot(markerS, c1921[:,0], label='T11.MQNEV019')
    plt.plot(markerS, c1921[:,1], label='T11.MQNEV021')
    plt.xlabel('Focus Location S (m)')
    plt.ylabel('Current (A)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('focus_currents.pdf')


    #[list(x) for x in zip(*table)],
    

def FitFocusToMarker(marker, s, quadsToVary, momentum, prefix, sequence="T11"):
    m = marker
    s = sequence
    p = momentum
    
    madx.input("match, rmatrix, sequence="+s+", betx=1, alfx=0, bety=1, alfy=0;")
    madx.input("constraint, sequence="+s+", range="+m+", RE12=0;")
    madx.input("constraint, sequence="+s+", range="+m+", RE34=0;")
    for q in quadsToVary:
        mTypeName = eamagparam.mapping.GetMagnetTypeName(q)
        mag = eamagparam.magnet_info.magnets[mTypeName]
        k1Max = mag.bliCurve.K1FromI(mag.bliCurve.maxCurrent, p)
        madx.input("vary, name="+q+"->K1, step=1e-3, lower=-"+str(k1Max)+", upper="+str(k1Max)+";")
    madx.input("jacobian, calls=5000, tolerance=1e-5;")
    madx.input("endmatch;")

    fn = prefix+m+".tfs"
    madx.input("twiss, chrom=true, rmatrix=true, betx=1.0, bety=1.0, save, file="+fn+";")
    return fn

    
if __name__ == "__main__":
    GenerateFocusPoints()
