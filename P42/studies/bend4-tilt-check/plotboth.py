import matplotlib.pyplot as _plt
import numpy as _np
import pymadx as _pymadx

def PlotXY():

    original = _pymadx.Data.Tfs("p42_2023_original.tfs")
    new = _pymadx.Data.Tfs("p42_2023_new.tfs")

    xo = original.GetColumn('X')
    yo = original.GetColumn('Y')
    zo = original.GetColumn('Z')

    xn = new.GetColumn('X')
    yn = new.GetColumn('Y')
    zn = new.GetColumn('Z')

    f = _plt.figure(figsize=(10,5))
    ax1 = f.add_subplot(211)

    ax1.plot(zo, xo, marker='.', label='original')
    ax1.plot(zn, xn, marker='.', label='new')
    _plt.xlabel('Z (m)')
    _plt.ylabel('X (m)')
    _plt.legend()

    ax2 = f.add_subplot(212)
    ax2.plot(zo, yo, marker='.', label='original')
    ax2.plot(zn, yn, marker='.', label='new')
    _plt.xlabel('Z (m)')
    _plt.ylabel('Y (m)')
    _plt.legend()

    _plt.tight_layout()



def PlotX():
    original = _pymadx.Data.Tfs("p42_2023_original.tfs")
    new = _pymadx.Data.Tfs("p42_2023_new.tfs")

    xo = original.GetColumn('X')
    yo = original.GetColumn('Y')
    zo = original.GetColumn('Z')

    xn = new.GetColumn('X')
    yn = new.GetColumn('Y')
    zn = new.GetColumn('Z')

    f = _plt.figure(constrained_layout=True, figsize=(10,10))
    n = 4
    gs = f.add_gridspec(nrows=n)
    ax1 = f.add_subplot(gs[:n-1, 0])
    ax2 = f.add_subplot(gs[n-1, 0], sharex=ax1)
    
    ax1.plot(zo, xo, marker='.', label='original')
    ax1.plot(zn, xn, marker='.', label='new')
    ax1.set_ylabel('X (m)')
    ax1.legend()
    #ax1.set_xticks([])
    _plt.setp(ax1.get_xticklabels(), visible=False)

    ax2.plot(zo, (xn-xo)*1000, marker='.')
    ax2.set_ylabel('Difference (mm)')
    
    _plt.xlabel('Z (m)')    
    ax1.set_ylim(-1.6,0.1)
    ax2.set_ylim(-100,100)
    _plt.xlim(0,200)

def PlotY():
    original = _pymadx.Data.Tfs("p42_2023_original.tfs")
    new = _pymadx.Data.Tfs("p42_2023_new.tfs")

    xo = original.GetColumn('X')
    yo = original.GetColumn('Y')
    zo = original.GetColumn('Z')

    xn = new.GetColumn('X')
    yn = new.GetColumn('Y')
    zn = new.GetColumn('Z')

    f = _plt.figure(constrained_layout=True, figsize=(10,10))
    n = 4
    gs = f.add_gridspec(nrows=n)
    ax1 = f.add_subplot(gs[:n-1, 0])
    ax2 = f.add_subplot(gs[n-1, 0], sharex=ax1)
    
    ax1.plot(zo, yo, marker='.', label='original')
    ax1.plot(zn, yn, marker='.', label='new')
    ax1.set_ylabel('Y (m)')
    ax1.legend()
    _plt.setp(ax1.get_xticklabels(), visible=False)

    ax2.plot(zo, (yn-yo)*1000, marker='.')
    ax2.set_ylabel('Difference (mm)')
    
    _plt.xlabel('Z (m)')    
    ax1.set_ylim(-0.01,2.0)
    ax2.set_ylim(-20,80)
    _plt.xlim(0,200)


def PlotZXAng():
    original = _pymadx.Data.Tfs("p42_2023_original.tfs")
    new = _pymadx.Data.Tfs("p42_2023_new.tfs")

    lo = original.GetColumn('L')
    xo = original.GetColumn('X')[lo!=0]
    zo = original.GetColumn('Z')[lo!=0]
    dxo = _np.diff(xo)
    dzo = _np.diff(zo)
    ango = _np.arctan(dxo/dzo)
    #ango[_np.isnan(ango)] = 0.0

    ln = original.GetColumn('L')
    xn = new.GetColumn('X')[ln!=0]
    zn = new.GetColumn('Z')[ln!=0]
    dxn = _np.diff(xn)
    dzn = _np.diff(zn)
    angn = _np.arctan(dxn/dzn)
    #angn[_np.isnan(angn)] = 0.0

    f = _plt.figure(constrained_layout=True, figsize=(10,10))
    n = 4
    gs = f.add_gridspec(nrows=n)
    ax1 = f.add_subplot(gs[:n-1, 0])
    ax2 = f.add_subplot(gs[n-1, 0], sharex=ax1)

    zm = (zo[:-1] + zo[1:])/2.0
    
    ax1.plot(zm, ango*1e3, marker='.', label='original')
    ax1.plot(zm, angn*1e3, marker='.', label='new')
    ax1.set_ylabel('Angle (mrad)')
    ax1.legend()
    #ax1.set_xticks([])
    _plt.setp(ax1.get_xticklabels(), visible=False)

    ax2.plot(zm, (angn-ango)*1e3, marker='.')
    ax2.set_ylabel('Difference (mrad)')
    
    _plt.xlabel('Z (m)')
    #_plt.ylabel('X (m)')
    #_plt.legend()

    #_plt.tight_layout()

    #_plt.subplots_adjust(vspace=0.01)
    
    ax1.set_ylim(-4,4)
    ax2.set_ylim(-3,3)
    _plt.xlim(0,100)



def TiltFractions(ang=6.5):

    x = _np.linspace(0.2,0.5,100)
    deg = (x/_np.pi)*180.0

    angx = ang*_np.cos(x)
    angy = ang*_np.sin(x)


    _plt.figure()
    _plt.plot(deg, angx, label='horizontal')
    _plt.plot(deg, angy, label='vertical')
    _plt.xlabel('Tilt Angle (degrees)')
    _plt.ylabel('Bending Angle (mrad)')
    _plt.legend()
