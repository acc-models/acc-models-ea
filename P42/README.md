P42 Optics
##########

2024-08-28 - Optics to use
--------------------------

Use this sequence file by default:
`STANDARD_P42_YETS 2022-2023.seq`
and this strength file:
`p42_2023.str`