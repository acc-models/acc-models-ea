import eautils
import pymadx


#a = pymadx.Data.Tfs("p42-cern-coordinate-frame-survey-bdf-v1.tfs")
#survey = eautils.survey.madx_survey_to_catia_by_name(a, 'EHN1-2704 (MBPL.022632.E)')
#survey.to_excel("p42-cern-coordinate-frame-survey-bdf-v1-catia.xlsx")

a = pymadx.Data.Tfs("p42-cern-coordinate-frame-survey-bdf-v2c.tfs")
survey = eautils.survey.madx_survey_to_catia_by_name(a, 'EHN1-2704 (MBPL.022632.E)')
survey.to_excel("p42-cern-coordinate-frame-survey-bdf-v2c-catia.xlsx")
