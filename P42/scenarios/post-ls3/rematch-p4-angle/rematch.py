import pymadx
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from cpymad.madx import Madx
import numpy as np

madx = Madx(stdout=False)

originalSurvey = pymadx.Data.Tfs("../../2024/p42-cern-coordinate-frame-survey.tfs")
# technically, QSL.X0430033 is the end of the first point we should be back on track
# but use a later point that will also work just in front of B4 so it works also for
# the 2nd option of the MSN just before B4
#matchPoint = "QSL.X0430033"
matchPoint = "QNL.X0430064"

goalX = originalSurvey[matchPoint]['X']
goalTheta = originalSurvey[matchPoint]['THETA']


def Fit():
    kB2 = 0.000484
    kB3 = 0.00312
    madx.input('call, file = "../p42.str";')
    madx.input('call, file = "../pos180-pos115-wobbling-ship.str";')
    madx.input('call, file = "../p42-transformation.str";')
    madx.input('call, file = "../p42.seq";')
    madx.input('beam, particle=proton, sequence=P42, pc=400.0;')

    result = minimize(FitObjective, [kB2, kB3], method='Nelder-Mead')
    print(result)
    print("kB2: {:.9f}".format(result.x[0]))
    print("kB3: {:.9f}".format(result.x[1]))
    

def FitObjective(kB):
    madx.input('kB2 := ' + str(kB[0]) + ';')
    madx.input('kB3 := ' + str(kB[1]) + ';')
    print('kB2 = ', str(kB[0]), 'kB3 = ', str(kB[1]))
    madx.input('use, sequence = P42;')
    madx.input('select, flag = survey, clear;')
    madx.input('survey, x0=-673.59173, y0=2441.5738, z0=4594.17888, theta0=-0.156550327, phi0=-0.00036, psi0=0.0, file="surveyoutput.tfs";')
    data = pymadx.Data.Tfs('surveyoutput.tfs')

    currentX = data[matchPoint]['X']
    currentTheta = data[matchPoint]['THETA']

    dx = np.abs(currentX - goalX)
    dxp = np.abs(currentTheta - goalTheta)

    return 1000.0 * dx + 1000.0 * dxp


if __name__ == '__main__':
    Fit()
