import pymadx
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 24, 'xtick.labelsize': 18, 'ytick.labelsize': 18})

def PlotSurvey():
    dP42_Wobbling = pymadx.Data.Tfs('P42_New_Wobbling.survey')
    dP42 = pymadx.Data.Tfs('P42_Old_Wobbling.survey')

    fig = plt.figure(figsize=(15, 9))
    ax = fig.add_subplot(111)

    ax.set_xlabel(r'$z$ in m')
    ax.set_ylabel(r'$x$ in m')

    ax.plot(dP42_Wobbling.GetColumn('Z'), dP42_Wobbling.GetColumn('X'), label='New P42', color='blue')
    ax.plot(dP42.GetColumn('Z'), dP42.GetColumn('X'), label='Old P42', color='red')

    ax.legend()

    pymadx.Plot.AddMachineLatticeToFigure(fig, 'P42_New_Wobbling.tfs')
    #ax.set_xlim(0, 91.30060102)

    plt.show()

if __name__ == '__main__':
    PlotSurvey()