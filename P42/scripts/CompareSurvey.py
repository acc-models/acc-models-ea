import pymadx
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from cpymad.madx import Madx
import numpy as np

plt.rcParams.update({'font.size': 24, 'xtick.labelsize': 18, 'ytick.labelsize': 18})

madx = Madx(stdout=False)

def FitMSN():
    kB2 = 0.000484
    kB3 = 0.00312
    madx.input('call, file = "p42_2023.str";')
    madx.input('call, file = "pos180-pos115-wobbling-ship.str";')
    madx.input('call, file = "new-P42-transformation.str";')
    madx.input('call, file = "STANDARD_P42_YETS 2022-2023.seq";')
    madx.input('beam, particle = proton, sequence = P42, pc = 400.0;')

    result = minimize(FitMSNObjective, [kB2, kB3], method='Nelder-Mead', callback=callbackMSN)
    print(result)
    print(kB2, kB3)

def callbackMSN(kB):
    fit = FitMSNObjective(kB)
    print('Objective fn = ', str(fit), end="\r")

def FitMSNObjective(kB):
    madx.input('kB2 := ' + str(kB[0]) + ';')
    madx.input('kB3 := ' + str(kB[1]) + ';')
    print('kB2 = ', str(kB[0]), 'kB3 = ', str(kB[1]))
    madx.input('use, sequence = P42;')
    madx.input('select, flag = survey, clear;')
    madx.input('select, flag = survey, column={name, s, l, angle, x, y, z, theta, phi, psi, globaltilt, slot_id, assembly_id};')
    madx.input('survey, x0=-673.59173, y0=2441.5738, z0=4594.17888, theta0=-0.156550327, phi0=-0.00036, psi0=0.0, file="surveyoutput.tfs";')
    data = pymadx.Data.Tfs('surveyoutput.tfs')

    xB4 = data.GetRowDict('DRIFT_19')['X']
    xpB4 = data.GetRowDict('DRIFT_19')['THETA']

    dx = np.abs(xB4 + 683.9781724)
    dxp = np.abs(xpB4 + 0.1593509678)

    return 1000.0 * dx + 1000.0 * dxp

def FitSHiP():
    kB11 = 0.007300
    kB12 = 0.0033351
    madx.input('call, file = "../scenarios/post-ls3/p42.str";')
    madx.input('call, file = "../scenarios/post-ls3/pos180-pos115-wobbling-ship.str";')
    madx.input('call, file = "../scenarios/post-ls3/p42-transformation.str";')
    madx.input('call, file = "../scenarios/post-ls3/p42.seq";')
    madx.input('beam, particle = proton, sequence = P42, pc = 400.0;')
    madx.input('set, format = "15.9f";')

    result = minimize(FitSHiPObjective, [kB11, kB12], method='Nelder-Mead', callback=callbackSHiP)
    print(result)

def callbackSHiP(kB):
    fit = FitSHiPObjective(kB)
    print('Objective fn = ', str(fit), end="\r")

def FitSHiPObjective(kB):
    madx.input('kB11 := ' + str(kB[0]) + ';')
    madx.input('kB12 := ' + str(kB[1]) + ';')
    print('kB11 = ', str(kB[0]), 'kB12 = ', str(kB[1]))
    madx.input('use, sequence = P42;')
    madx.input('select, flag = survey, clear;')
    madx.input('select, flag = survey, column={name, s, l, angle, x, y, z, theta, phi, psi, globaltilt, slot_id, assembly_id};')
    madx.input('survey, x0=-673.59173, y0=2441.5738, z0=4594.17888, theta0=-0.156550327, phi0=-0.00036, psi0=0.0, file="surveyoutput.tfs";')
    data = pymadx.Data.Tfs('surveyoutput.tfs')

    position = 'ENDP42'

    dx = np.abs(data.GetRowDict(position)['X'] + 829.171135470)
    dxp = np.abs(data.GetRowDict(position)['THETA'] + 0.124639891)
    dy = np.abs(data.GetRowDict(position)['Y'] - 2448.749238421)
    dyp = np.abs(data.GetRowDict(position)['PHI'])

    return 1e6 * (dx + dxp + dy + dyp)

def CompareSurvey():
    d_current = pymadx.Data.Tfs('P42_Current_Survey.tfs')
    d_post_ls3 = pymadx.Data.Tfs('P42_Post_LS3_Move_MSN2_Survey.tfs')

    x_current = d_current.GetColumn('X')
    y_current = d_current.GetColumn('Y')
    z_current = d_current.GetColumn('Z')

    x_post_ls3 = d_post_ls3.GetColumn('X')
    y_post_ls3 = d_post_ls3.GetColumn('Y')
    z_post_ls3 = d_post_ls3.GetColumn('Z')

    figXZ = plt.figure()
    axXZ = figXZ.add_subplot(111)
    axXZ.plot(z_current, x_current, marker='.', label='Current')
    axXZ.plot(z_post_ls3, x_post_ls3, marker='.', label='Post LS3')
    #pymadx.Plot.AddMachineLatticeToFigure(figXZ, 'p42_2023_Ref_Magnets.twiss')
    axXZ.set_xlabel(r'$z$ in m')
    axXZ.set_xlim(0.0, 100.0)
    axXZ.set_ylabel(r'$x$ in m')
    axXZ.set_ylim(-0.7, 0.05)
    axXZ.legend()
    figXZ.tight_layout()

    figYZ = plt.figure()
    axYZ = figYZ.add_subplot(111)
    axYZ.plot(z_current, y_current, marker='.', label='Current')
    axYZ.plot(z_post_ls3, y_post_ls3, marker='.', label='Post LS3')
    #pymadx.Plot.AddMachineLatticeToFigure(figYZ, 'p42_2023_Ref_Magnets.twiss')
    axYZ.set_xlabel(r'$z$ in m')
    axYZ.set_xlim(0.0, 100.0)
    axYZ.set_ylabel(r'$y$ in m')
    axYZ.set_ylim(-0.005, 0.17)
    axYZ.legend()
    figYZ.tight_layout()

if __name__ == '__main__':
    CompareSurvey()
    plt.show()